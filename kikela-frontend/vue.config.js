const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  outputDir: 'target/dist-' + process.env.DIST_FOLDER,
  transpileDependencies: true
})
