#!/bin/bash

DBdir=/home/postgresql-15
DB=kikela
docker run \
  --name postgres-15-${DB} \
  --rm \
  -v ${DBdir}-${DB}:/var/lib/postgresql/data \
  -e POSTGRES_DB=${DB} \
  -e POSTGRES_PASSWORD=whatever \
  -p 25433:5432 \
  -d postgres:15
