package org.chorem.kikela;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import io.agroal.api.AgroalDataSource;
import org.chorem.kikela.entities.Tables;
import org.chorem.kikela.entities.tables.daos.KikelaUserDao;
import org.chorem.kikela.entities.tables.daos.PresenceDao;
import org.chorem.kikela.entities.tables.pojos.KikelaUser;
import org.chorem.kikela.entities.tables.pojos.Presence;
import org.jboss.logging.Logger;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KikelaResource {

    @Inject
    protected AgroalDataSource dataSource;

    protected Configuration jooqConfiguration;

    @Inject
    protected Logger log;

    protected Configuration getJooqConfiguration() {
        if (jooqConfiguration == null) {
            jooqConfiguration = DSL.using(dataSource, SQLDialect.POSTGRES).configuration();
        }
        return jooqConfiguration;
    }

    protected PresenceDao getPresenceDao() {
        return new PresenceDao(getJooqConfiguration());
    }

    protected KikelaUserDao getKikelaUserDao() {
        return new KikelaUserDao(getJooqConfiguration());
    }

    @GET
    @Path("/now")
    public WeekAndYear now() {
        LocalDate now = LocalDate.now();
        // Si on est le dimanche, on affiche la semaine à venir
        if (DayOfWeek.SUNDAY.equals(now.getDayOfWeek())) {
            now = now.plusDays(1);
        }
        int week = now.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR);
        int year = now.get(IsoFields.WEEK_BASED_YEAR);
        WeekAndYear result = new WeekAndYear(week, year);
        return result;
    }

    @GET
    @Path("/users")
    public List<KikelaUser> listAllUsers() {
        List<KikelaUser> users = getKikelaUserDao().findAll();
        List<KikelaUser> result = Ordering.natural().onResultOf(KikelaUser::getTrigramme).sortedCopy(users);
        return result;
    }

    @GET
    @Path("/days")
    public List<LocalDate> listWeekDays(@QueryParam("week") int week, @QueryParam("year") int year) {
        LocalDate monday = LocalDate.ofYearDay(year, 50)
                .with(IsoFields.WEEK_OF_WEEK_BASED_YEAR, week)
                .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        List<LocalDate> result = new LinkedList<>();
        result.add(monday);
        result.add(monday.with(TemporalAdjusters.next(DayOfWeek.TUESDAY)));
        result.add(monday.with(TemporalAdjusters.next(DayOfWeek.WEDNESDAY)));
        result.add(monday.with(TemporalAdjusters.next(DayOfWeek.THURSDAY)));
        result.add(monday.with(TemporalAdjusters.next(DayOfWeek.FRIDAY)));
        return result;
    }

    @GET
    @Path("/presences")
    public List<Presence> listAll() {
        List<Presence> result = getPresenceDao().findAll();
        return result;
    }

    @GET
    @Path("/presences-by-week")
    public Multimap<LocalDate, String> findByWeek(@QueryParam("week") int week, @QueryParam("year") int year, @QueryParam("isPresent") boolean isPresent) {
        LocalDate monday = LocalDate.ofYearDay(year, 50)
                .with(IsoFields.WEEK_OF_WEEK_BASED_YEAR, week)
                .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate friday = monday.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
        List<Presence> presences = getPresenceDao().ctx()
                .selectFrom(Tables.PRESENCE)
                .where(Tables.PRESENCE.DAY.between(monday, friday))
                .and(Tables.PRESENCE.IS_PRESENT.eq(isPresent))
                .fetchInto(Presence.class);
        ImmutableListMultimap<LocalDate, Presence> presencesByDay = Multimaps.index(presences, Presence::getDay);
        ListMultimap<LocalDate, String> result = Multimaps.transformValues(presencesByDay, Presence::getUserId);
        return result;
    }

    @DELETE
    @Path("/presences/{trigramme}/{day}")
    public void removePresence(@PathParam("day") LocalDate day, @PathParam("trigramme") String trigramme) {
        Presence presence = new Presence(trigramme, day, false);
        getPresenceDao().delete(presence);
    }

    @PUT
    @Path("/presences/{trigramme}/{day}/{isPresent}")
    public void setPresence(@PathParam("day") LocalDate day, @PathParam("trigramme") String trigramme, @PathParam("isPresent") Boolean isPresent) {
        Presence presence = new Presence(trigramme, day, isPresent);
        PresenceDao presenceDao = getPresenceDao();
        if (!presenceDao.exists(presence)) {
            presenceDao.insert(presence);
        } else {
            presenceDao.update(presence);
        }
    }

    @PUT
    @Path("/presences/{trigramme}/{year}/{week}/{isPresent}")
    public void setPresenceWeek(@PathParam("year") int year, @PathParam("week") int week, @PathParam("trigramme") String trigramme, @PathParam("isPresent") Boolean isPresent) {
        List<LocalDate> weekDays = listWeekDays(week, year);
        weekDays.forEach(day -> this.setPresence(day, trigramme, isPresent));
    }

    @DELETE
    @Path("/presences/{trigramme}/{year}/{week}")
    public void removePresenceWeek(@PathParam("year") int year, @PathParam("week") int week, @PathParam("trigramme") String trigramme) {
        PresenceDao presenceDao = getPresenceDao();
        List<LocalDate> weekDays = listWeekDays(week, year);
        weekDays.forEach(day -> {
            Presence presence = new Presence(trigramme, day, false);
            if (presenceDao.exists(presence)) {
                presenceDao.delete(presence);
            }
        });
    }
}
