package org.chorem.kikela;

public record WeekAndYear(int week, int year) {
}
