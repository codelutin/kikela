package org.chorem.kikela;

import org.jboss.logging.Logger;

import jakarta.inject.Inject;
import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.Provider;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Provider
public class LocalDateParamConverter implements ParamConverter<LocalDate> {

    @Inject
    protected Logger log;

    public LocalDate fromString(String value) {
        try {
            return LocalDate.parse(value);
        } catch (DateTimeParseException e) {
            log.warn("Unable to parse date: " + value, e);
        }
        return null;
    }

    public String toString(LocalDate value) {
        return value.toString();
    }

}
