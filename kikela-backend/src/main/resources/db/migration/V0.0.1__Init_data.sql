---
-- %%Ignore-License
---

INSERT INTO kikela_user (trigramme, first_name) VALUES
('AT', 'Arnaud'),
('LK', 'Léo'),
('CB', 'Cécilia'),
('DC', 'David'),
('ALS', 'Aurélie'),
('KM', 'Kevin'),
('BLN', 'Brendan'),
('SB', 'Sylvain'),
('BP', 'Benjamin'),
('ND', 'Noëmie'),
('EG', 'Estelle'),
('JC', 'Jean'),
('AC', 'Adrien'),
('EC', 'Éric'),
('RG', 'Rémi'),
('SM', 'Samuel'),
('AMA', 'Andrés'),
('AMO', 'Alex'),
('ALE', 'Aurore'),
('GG', 'Geoffroy')
;

INSERT INTO kikela_user (trigramme, first_name, last_name) VALUES
('JR', 'Julien', 'Ruchaud'),
('JG', 'Julien', 'Gaston')
;
