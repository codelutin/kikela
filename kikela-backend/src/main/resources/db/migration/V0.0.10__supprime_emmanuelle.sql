-- Active les cascade, quand on supprimer un lutin, on supprime sa présence

ALTER TABLE presence DROP constraint presence_user_id_fkey;
ALTER TABLE presence ADD FOREIGN KEY (user_id) REFERENCES kikela_user(trigramme) ON DELETE CASCADE;

-- Supprime Emmanuelle
DELETE FROM kikela_user WHERE trigramme = 'EB';
