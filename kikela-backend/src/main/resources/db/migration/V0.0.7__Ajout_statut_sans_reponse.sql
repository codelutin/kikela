---
-- %%Ignore-License
---

ALTER TABLE presence ADD COLUMN "is_present" BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE presence SET is_present = TRUE WHERE DAY IS NOT NULL;
