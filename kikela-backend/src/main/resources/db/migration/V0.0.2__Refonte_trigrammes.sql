---
-- %%Ignore-License
---

INSERT INTO kikela_user (trigramme, first_name) VALUES
('ALO', 'Aurélie')
;

UPDATE presence SET user_id = 'ALO' WHERE user_id = 'ALS';

DELETE FROM kikela_user WHERE trigramme = 'ALS';
