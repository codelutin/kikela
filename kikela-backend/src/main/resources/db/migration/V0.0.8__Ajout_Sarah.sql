---
-- %%Ignore-License
---

INSERT INTO kikela_user (trigramme, first_name) VALUES
  ('SBO', 'Sarah'),
  ('SBA', 'Sylvain');

UPDATE presence set user_id = 'SBA' where user_id = 'SB';

DELETE FROM kikela_user where trigramme = 'SB';