---
-- %%Ignore-License
---

DELETE FROM presence WHERE user_id = 'MR';
DELETE FROM kikela_user WHERE trigramme = 'MR';

INSERT INTO kikela_user (trigramme, first_name) VALUES
  ('AF', 'Aurore');
INSERT INTO kikela_user (trigramme, first_name) VALUES
  ('AL', 'Aurélie');

UPDATE presence SET user_id='AF'
  WHERE user_id='ALE';
UPDATE presence SET user_id='AL'
  WHERE user_id='ALO';

DELETE FROM kikela_user WHERE trigramme = 'ALE';
DELETE FROM kikela_user WHERE trigramme = 'ALO';
