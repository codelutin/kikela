---
-- %%Ignore-License
---

CREATE TABLE kikela_user (
    trigramme VARCHAR(3) NOT NULL PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name TEXT
);

CREATE TABLE presence (
    user_id VARCHAR(3) NOT NULL REFERENCES kikela_user(trigramme),
    day DATE NOT NULL,
    PRIMARY KEY(user_id, day)
);
